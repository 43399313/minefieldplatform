from operator import truediv
from minefieldplatform.celery import app
from cmdb.models import StorageMachine, GPUMachine


@app.task(name='更新服务器资产信息')
def update_hosts_from_minefield():
    with open('/tmp/newstorage.txt') as lines:
        for line in lines:
            line = line.strip().split(';')
            if StorageMachine.objects.filter(ip=line[0]).count():
                StorageMachine.objects.filter(ip=line[0]).update(cpu_cores=line[1], total_memory=line[2],
                                                                 disk=line[3], area=line[4], node_status=line[5], current_file=line[6], capacity=line[7])
            else:
                storage = StorageMachine(ip=line[0], cpu_cores=line[1], total_memory=line[2],
                                         disk=line[3], area=line[4], node_status=line[5], current_file=line[6], capacity=line[7])
                storage.save()
    with open('/tmp/newgpu.txt') as lines:
        for line in lines:
            line = line.strip().split(';')
            if GPUMachine.objects.filter(ip=line[0]).count():
                GPUMachine.objects.filter(ip=line[0]).update(cpu_cores=line[1], total_memory=line[2],
                                                             gpu_numbers=line[3], mount_with=line[4], area=line[5], node_status=line[6])
            else:
                gpu = GPUMachine(ip=line[0], cpu_cores=line[1], total_memory=line[2],
                                 gpu_numbers=line[3], mount_with=line[4], area=line[5], node_status=line[6])
                gpu.save()
