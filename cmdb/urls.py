from django.urls import path, re_path
from .views import *

app_name = 'cmdb'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('storage_list/', StorageListView.as_view(), name='storagelist'),
    path('gpu_list/', GPUListView.as_view(), name='gpulist'),
    re_path('storage_del/(?P<pk>[0-9]+)?/', StorageDeleteView.as_view(), name='storagedel'),
    re_path('gpu_del/(?P<pk>[0-9]+)?/', GPUDeleteView.as_view(), name='gpudel'),
    re_path('storage_modify/(?P<pk>[0-9]+)?/', StorageUpdateView.as_view(), name='storagemodify'),
    re_path('gpu_modify/(?P<pk>[0-9]+)?/', GPUUpdateView.as_view(), name='gpumodify'),
    path('overview/', AssetsOverView.as_view(), name='overview'),
    path('update_host_info/', update_host_info, name='update-host-info'),
]
