from django.db import models


class GPUMachine(models.Model):
    ip = models.GenericIPAddressField(verbose_name='gpu机器ip地址')
    cpu_cores = models.IntegerField(
        verbose_name='cpu核心数，单位：核', null=True, blank=True)
    total_memory = models.IntegerField(
        verbose_name='内存大小，单位：G', null=True, blank=True)
    gpu_numbers = models.IntegerField(
        verbose_name='GPU数量，单位：个', null=True, blank=True)
    mount_with = models.CharField(
        verbose_name='挂载的存储机器ip', null=True, blank=True, max_length=200)
    area = models.CharField(verbose_name='所属矿场',
                            max_length=10, null=True, blank=True)
    node_status = models.CharField(
        verbose_name='运行状态', max_length=10, null=True, blank=True)

    class Meta:
        verbose_name = 'GPU机器信息'

    def __str__(self):
        return self.ip


class StorageMachine(models.Model):
    ip = models.GenericIPAddressField(verbose_name='存储机器ip地址')
    cpu_cores = models.IntegerField(
        verbose_name='cpu核心数，单位：核', null=True, blank=True)
    total_memory = models.IntegerField(
        verbose_name='内存大小，单位：G', null=True, blank=True)
    disk = models.IntegerField(verbose_name='硬盘数量，单位：块', null=True, blank=True)
    area = models.CharField(verbose_name='所属矿场',
                            max_length=10, null=True, blank=True)
    node_status = models.CharField(
        verbose_name='机器运行状态', max_length=10, null=True, blank=True)
    current_file = models.IntegerField(
        verbose_name='当前P盘文件号', blank=True, default=0)
    capacity = models.DecimalField(
        verbose_name='可提供算力', blank=True, default=0, decimal_places=3, max_digits=20)

    class Meta:
        verbose_name = '存储机器信息'

    def __str__(self):
        return self.ip
