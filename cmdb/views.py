from django.db.models import Q
from django.shortcuts import render, reverse
from django.views.generic import View, ListView, DeleteView, UpdateView
from cmdb.models import StorageMachine, GPUMachine
from pure_pagination import PaginationMixin
from .summary import assets_overview, index_overview
from django.views.decorators.http import require_GET
from django.http import HttpResponse, JsonResponse
from .tasks import update_hosts_from_minefield


class IndexView(View):
    @staticmethod
    def get(request):
        overview = index_overview()
        context = {
            'overview': overview,
        }
        return render(request, 'index.html', context=context)

    @staticmethod
    def post(request):
        print("hello")
        return render(request, 'index.html')


class StorageListView(PaginationMixin, ListView):
    template_name = "cmdb/storagelist.html"
    model = StorageMachine
    paginate_by = 10
    keyword = ""

    def get_keyword(self):
        self.keyword = self.request.GET.get('keyword')
        return self.keyword

    # 数据过滤
    def get_queryset(self):
        queryset = super().get_queryset()
        keyword = self.get_keyword()
        if keyword:
            queryset = queryset.filter(Q(ip__icontains=keyword) |
                                       Q(area__contains=keyword) |
                                       Q(node_status__contains=keyword))

        return queryset

    # 搜索关键字传给前端
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['keyword'] = self.keyword
        return context


class GPUListView(PaginationMixin, ListView):
    template_name = "cmdb/gpulist.html"
    model = GPUMachine
    paginate_by = 10
    keyword = ""

    def get_keyword(self):
        self.keyword = self.request.GET.get('keyword')
        return self.keyword

    # 数据过滤
    def get_queryset(self):
        queryset = super().get_queryset()
        keyword = self.get_keyword()
        if keyword:
            queryset = queryset.filter(Q(ip__icontains=keyword) |
                                       Q(area__contains=keyword) |
                                       Q(node_status__contains=keyword) |
                                       Q(mount_with__icontains=keyword))

        return queryset

    # 搜索关键字传给前端
    def get_context_data(self, **kwargs):
        context = super(GPUListView, self).get_context_data(**kwargs)
        context['keyword'] = self.keyword
        return context


class StorageDeleteView(DeleteView):
    template_name = 'cmdb/storage_delete_confirm.html'
    model = StorageMachine

    def get_success_url(self):
        return reverse('cmdb:storagelist')


class GPUDeleteView(DeleteView):
    template_name = 'cmdb/gpu_delete_confirm.html'
    model = GPUMachine

    def get_success_url(self):
        return reverse('cmdb:gpulist')


class StorageUpdateView(UpdateView):
    template_name = "cmdb/storageform.html"
    model = StorageMachine
    fields = ('ip', 'cpu_cores', 'total_memory', 'disk', 'area', 'node_status')

    def get_success_url(self):
        return reverse('cmdb:storagelist')


class GPUUpdateView(UpdateView):
    template_name = "cmdb/gpuform.html"
    model = GPUMachine
    fields = ('ip', 'cpu_cores', 'total_memory',
              'gpu_numbers', 'area', 'node_status')

    def get_success_url(self):
        return reverse('cmdb:gpulist')


class AssetsOverView(View):
    @staticmethod
    def get(request):
        overview = assets_overview()
        context = {
            'overview': overview,
        }
        return render(request, 'cmdb/assets_overview.html', context=context)


@require_GET
def update_host_info(request):
    """
    更新主机信息
    :param request:
    :return:
    """
    # update_hosts_from_minefield.delay()
    update_hosts_from_minefield()
    return HttpResponse('任务已提交到后台，请稍等片刻！')
