from django.db import models


class Task(models.Model):
    STATUS = (
        (0, '未发布'),
        (1, '已发布'),
        (2, '已取消'),
    )
    name = models.CharField(max_length=100, verbose_name='任务名')
    dest_hosts = models.TextField(verbose_name='目标主机')
    content = models.TextField(verbose_name='更新内容', null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    status = models.IntegerField(default=0, choices=STATUS)
    result = models.TextField(null=True, verbose_name='执行结果')

    class Meta:
        verbose_name = '任务系统'
        verbose_name_plural = '任务系统'

    def __str__(self):
        return self.name
