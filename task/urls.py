from django.urls import path, re_path
from .views import *

app_name = 'task'

urlpatterns = [
    path('add/', TaskAddView.as_view(), name='task_add'),
    path('get_playbook/', get_playbook, name='get_playbook'),
    path('list/', TaskListView.as_view(), name='task_list'),
    re_path('detail/(?P<pk>[0-9]+)?/', TaskDetailView.as_view(), name='task_detail'),
    re_path('cancel/(?P<pk>[0-9]+)?/', TaskCancelView.as_view(), name='task_cancel'),
    path('get_task_result/', get_task_result, name='get_task_result'),
    re_path('publish/(?P<pk>[0-9]+)?/', task_publish, name='task_publish'),
]
