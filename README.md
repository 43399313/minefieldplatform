# minefieldplatform

## 简介

公司自用矿场管理平台，运维专用，已上线运行一段时间

## 部署注意事项

* 项目采用nginx+gunicorn+supervisor的方式部署
* 部署前要创建数据库和表结构，并启动redis
* 要特别注意提前将所有机器之间做好ssh免密认证

## 项目技术栈

* 项目采用python 3.6，django 2.2开发，传统MTV模式
* 用到的其它组件有mysql，redis，celery，nginx，ansible
* 项目的主要功能有CMDB和TASK模块

## 仪表盘

显示仪表盘信息

![index](/image/index.png)

## CMDB

用于管理矿场资产，有资产总览页面和机器信息页面

#### 资产总览

![overview](/image/overview.png)

* 该页面显示资产概览，标签云可以通过点击某标签，快速过滤出故障和已停止机器

#### 机器信息

![machinelist](/image/machinelist.png)
![machinelist](/image/gpu.png)

* 该页面显示详细资产，包括存储机器和GPU机器。可以从页面上进行修改和删除操作，但是无法进行新增操作
* 新增操作采用的是模拟自动发现功能。当机器上线时，用初始化脚本跑定时任务，每10分钟将自己的详细信息(包括硬件信息和plot信息等)发送到跳板机上特定文件夹内(例如/tmp/storage和/tmp/gpu)，以IP地址命名文件，再由跳板机每隔10分钟扫描该文件夹，并做处理后集中到特定文件内，再传送到项目所在机器的/tmp/目录内。平台会定时从文件中读取到每一个机器信息，并更新数据库。

## TASK

用于运维发布任务，以及查看任务详情

#### 通用任务

![task](/image/task.png)

* 结合ansible api，用playbook来完成任务发布。playbook可以自定义，多个IP地址用逗号分隔。
* 创建任务后，点击保存，不会执行，会将该任务保存到任务列表中，需要到任务列表中对任务进行操作

#### 任务列表

![tasklist](/image/tasklist.png)
![taskdetail](/image/taskdetail.png)

* 任务未发布时，可以点击查看按钮，进入任务详情，进行发布任务或者取消任务
* 任务取消后，可以点击查看按钮，进入任务详情，查看任务详情
* 任务发布后，可以点击查看按钮，进入任务详情，查看任务详情

## roadmap

项目后期规划如下：

* 第一是按照公司要求，更新矿池管理模块
* 第二是增加矿池用户认证功能
